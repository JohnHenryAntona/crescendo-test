# Crescendo Collective - Frontend Skill Test

## TEST 1 ) Create JavaScript application

We provide a mock api listing recipe information along with ingredients that are on sale. Using
this information you create an app to browse and view recipes using a modern JavaScript
approach. No designs are provided so you are free to create your own layout or leverage a
framework for the design aspects.

Grab the API here: https://bitbucket.org/crescendocollective/frontend-api-skills-test

## TEST 2) Create HTML from a design document

We provide a mobile and desktop design for a fake company that you will convert into a
working responsive page. This should work on recent browsers and across mobile devices.

Grab PSDs: https://cloud.crescendocollective.com:5001/sharing/sRzEH9kmz
Password: crescendo-test