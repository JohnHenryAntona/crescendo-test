function isMobile() {
    var isMobile = false;
    if (Math.max(jQuery(window).width(), window.innerWidth) <= 768) {
        isMobile = true;
    }
    return isMobile;
}

$(document).ready(function() {

  	if (isMobile()) {
        // Related Products, Upsell, Crossell
        $('.insights-wrapper, .events-wrapper').find('.insights-list, .events-list').addClass('owl-carousel').owlCarousel({
            loop: false,
            autoplay: false,
            dots: true,
            margin: 25,
            responsiveClass: true,
            responsive: {
                0:{
                    items: 1,
                    nav: false,
                    margin: 0
                }
            }
        });

        $('.burger-menu-wrapper').on('click', function(){
        	$('.navigation').toggle();
        	$('header').toggleClass('burger-menu-toggle');
        	$('#overlay').toggle();
        });
    }
});