import React from 'react';
import {
    Container,
    Box,
    makeStyles,
    Grid
} from "@material-ui/core";

import AppBar from "./components/AppBar";
import RecipesTable from "./components/Recipes/RecipesTable";
import "./App.css"

const classes = makeStyles(theme => ({
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
    paddingTop: "4rem"
  },
  appBarSpacer: theme.mixins.toolbar,
  title: {
    flexGrow: 1
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  }
}));

export default function App() {
    return (
        <div>
            <AppBar/>
            <main className={classes.content}>
                <div className={classes.appBarSpacer} />
                <Container maxWidth="lg" className={classes.container}>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <Box>
                                <RecipesTable />
                            </Box>
                        </Grid>
                    </Grid>
                </Container>
            </main>
        </div>
    );
}
