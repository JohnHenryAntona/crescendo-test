import axios from 'axios';

export const apiUrl = 'http://localhost:3001';

export async function getRecipes() {
	const response = await axios.get(`${apiUrl}/recipes`);
	return response.data;
}

export const getRecipe = async(id) => {
	const response = await axios.get(`${apiUrl}/recipes/${id}`);
	return response.data;
}

export async function getSpecials() {
	const response = await axios.get(`${apiUrl}/specials`);
	return response.data;
}

export async function updateRecipe(values, id) {
	const response = await axios.patch(`${apiUrl}/recipes/${id}`, JSON.parse(values));
	return response.data;
}

export async function createRecipe(values) {
	const response = await axios.post(`${apiUrl}/recipes`, JSON.parse(values));
	return response.data;
}

// export async function postRecipe(values, id) {
// 	const response = fetch(`${apiUrl}/recipes/` + id, {
//       method: "POST",
//       headers: {
//         "Content-Type": "application/json"
//       },
//       body: JSON.parse(values)
//     })
  //   await axios.post(
		// {
		// 	url: 'http://localhost:3001/recipes',
		// 	method: 'POST',
		// 	body: JSON.parse(values),
		// 	headers: {
  //           	"Content-type": "application/json; charset=UTF-8"
  //   		}
  //       })
// 		.then(response => console.log(response))
//         .then(json => console.log(json));
// 	return response;
// }

export const deleteRecipe = async(id) => {
	return await axios.delete(`${apiUrl}/recipes/${id}`);
}