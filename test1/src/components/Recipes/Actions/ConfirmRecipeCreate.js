import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import Alert from '@material-ui/lab/Alert';
import Collapse from '@material-ui/core/Collapse';

import { createRecipe } from "../../../services/Api";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function ConfirmRecipeCreate(props) {
    const classes = useStyles();
    const { confirmCreateRecipeDialogIsOpen, setConfirmCreateRecipeDialogIsOpen, createFormData, setCreateDialogIsOpen, renewRecipesData } = props;

    const [fullWidth] = useState(true);
    const [confirmCreateDialogMaxWidth] = useState('xs');
    const [showCreateSuccessAlert, setShowCreateSuccessAlert] = useState(false);


    const handleConfirmCreateRecipe = (createFormData) => {
        (async () => {
            createRecipe(JSON.stringify(createFormData)).then((items) => {});
            setConfirmCreateRecipeDialogIsOpen(false);
            setCreateDialogIsOpen(false);
            setShowCreateSuccessAlert(true);
            renewRecipesData();
            setTimeout(
                () => setShowCreateSuccessAlert(false), 
                3000
            );
        })();
    };

    const handleConfirmCreateRecipeDialogClose = (event, row) => {
        setConfirmCreateRecipeDialogIsOpen(false);
    };

    return (
        <div className={classes.root}>
            <Dialog
                fullWidth={fullWidth}
                maxWidth={confirmCreateDialogMaxWidth}
                open={confirmCreateRecipeDialogIsOpen}
                onClose={handleConfirmCreateRecipeDialogClose}
                aria-labelledby="max-width-dialog-title"
            >
                <DialogTitle disableTypography>
                    <Typography variant="h6">Create Recipe</Typography>
                    <DialogContentText>
                        Are you sure you want to create the recipe {createFormData.title}?
                    </DialogContentText>
                </DialogTitle>
                <DialogContent>
                    
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleConfirmCreateRecipeDialogClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={(event) => handleConfirmCreateRecipe(createFormData)} color="primary">
                        Yes
                    </Button>
                </DialogActions>
            </Dialog>
            <Collapse in={showCreateSuccessAlert}>
                <Alert severity="success">You successfully updated a recipe!</Alert>
            </Collapse>
        </div>
    );
}

ConfirmRecipeCreate.propTypes = {
    confirmCreateRecipeDialogIsOpen: PropTypes.bool,
    setConfirmCreateRecipeDialogIsOpen: PropTypes.func,
    ucreateormData: PropTypes.object,
    setCreateDialogIsOpen: PropTypes.func,
    renewRecipesData: PropTypes.func
};
