import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import Alert from '@material-ui/lab/Alert';
import Collapse from '@material-ui/core/Collapse';

import { updateRecipe } from "../../../services/Api";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function ConfirmRecipeUpdate(props) {
    const classes = useStyles();
    const { confirmUpdateRecipeDialogIsOpen, setConfirmUpdateRecipeDialogIsOpen, updateFormData, setUpdateDialogIsOpen } = props;

    const [fullWidth] = useState(true);
    const [confirmUpdateDialogMaxWidth] = useState('xs');
    const [showUpdateSuccessAlert, setShowUpdateSuccessAlert] = useState(false);


    const handleConfirmUpdateRecipe = (updateFormData) => {
        (async () => {
            updateRecipe(JSON.stringify(updateFormData), updateFormData.uuid).then((items) => {});
            setConfirmUpdateRecipeDialogIsOpen(false);
            setUpdateDialogIsOpen(false);
            setShowUpdateSuccessAlert(true);
            setTimeout(
                () => setShowUpdateSuccessAlert(false), 
                3000
            );
        })();
    };

    const handleConfirmUpdateRecipeDialogClose = (event, row) => {
        setConfirmUpdateRecipeDialogIsOpen(false);
    };

    return (
        <div className={classes.root}>
            <Dialog
                fullWidth={fullWidth}
                maxWidth={confirmUpdateDialogMaxWidth}
                open={confirmUpdateRecipeDialogIsOpen}
                onClose={handleConfirmUpdateRecipeDialogClose}
                aria-labelledby="max-width-dialog-title"
            >
                <DialogTitle disableTypography>
                    <Typography variant="h6">Update Recipe</Typography>
                    <DialogContentText>
                        Are you sure you want to update the recipe {updateFormData.title}?
                    </DialogContentText>
                </DialogTitle>
                <DialogContent>
                    
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleConfirmUpdateRecipeDialogClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={(event) => handleConfirmUpdateRecipe(updateFormData)} color="primary">
                        Yes
                    </Button>
                </DialogActions>
            </Dialog>
            <Collapse in={showUpdateSuccessAlert}>
                <Alert severity="success">You successfully updated a recipe!</Alert>
            </Collapse>
        </div>
    );
}

ConfirmRecipeUpdate.propTypes = {
    confirmUpdateRecipeDialogIsOpen: PropTypes.bool,
    setConfirmUpdateRecipeDialogIsOpen: PropTypes.func,
    updateFormData: PropTypes.object,
    setUpdateDialogIsOpen: PropTypes.func,
};
