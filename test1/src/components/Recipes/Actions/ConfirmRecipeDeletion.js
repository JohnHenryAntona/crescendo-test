import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import Alert from '@material-ui/lab/Alert';
import Collapse from '@material-ui/core/Collapse';

import { getRecipes, deleteRecipe } from "../../../services/Api";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function ConfirmRecipeDeletion(props) {
    const classes = useStyles();
    const { confirmDeleteRecipeDialogIsOpen, setConfirmDeleteRecipeDialogIsOpen, confirmDeleteRecipeDialogData, setRecipesData, renewRecipesData } = props;

    const [fullWidth] = useState(true);
    const [confirmDeleteDialogMaxWidth] = useState('xs');
    const [showDeleteSuccessAlert, setShowDeleteSuccessAlert] = useState(false);


    const handleConfirmDeleteRecipe = (row) => {
        (async () => {
            deleteRecipe(row.uuid).then((items) => {});
            getRecipes().then((items) => {setRecipesData(items);});
            setConfirmDeleteRecipeDialogIsOpen(false);
            setShowDeleteSuccessAlert(true);
            renewRecipesData();
            setTimeout(
                () => setShowDeleteSuccessAlert(false), 
                3000
            );
        })();
    };

    const handleConfirmDeleteRecipeDialogClose = (event, row) => {
        setConfirmDeleteRecipeDialogIsOpen(false);
    };

    return (
        <div className={classes.root}>
            <Dialog
                fullWidth={fullWidth}
                maxWidth={confirmDeleteDialogMaxWidth}
                open={confirmDeleteRecipeDialogIsOpen}
                onClose={handleConfirmDeleteRecipeDialogClose}
                aria-labelledby="max-width-dialog-title"
            >
                <DialogTitle disableTypography>
                    <Typography variant="h6">Delete Recipe</Typography>
                    <DialogContentText>
                        Are you sure you want to delete this recipe {confirmDeleteRecipeDialogData.title}?
                    </DialogContentText>
                </DialogTitle>
                <DialogContent>
                    
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleConfirmDeleteRecipeDialogClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={(event) => handleConfirmDeleteRecipe(confirmDeleteRecipeDialogData)} color="primary">
                        Yes
                    </Button>
                </DialogActions>
            </Dialog>
            <Collapse in={showDeleteSuccessAlert}>
                <Alert severity="success">You successfully deleted a recipe!</Alert>
            </Collapse>
        </div>
    );
}

ConfirmRecipeDeletion.propTypes = {
    confirmDeleteRecipeDialogIsOpen: PropTypes.bool,
    setConfirmDeleteRecipeDialogIsOpen: PropTypes.func,
    confirmDeleteRecipeDialogData: PropTypes.object,
    setRecipesData: PropTypes.func,
    renewRecipesData: PropTypes.func
};
