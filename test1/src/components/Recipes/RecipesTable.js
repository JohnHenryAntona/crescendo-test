import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

import Paper from '@material-ui/core/Paper';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import VisibilityIcon from '@material-ui/icons/Visibility';
import EditIcon from '@material-ui/icons/Edit';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';

import { getRecipes, getSpecials } from "../../services/Api";
import EnhancedTableToolbar from "../Recipes/EnhancedTableToolbar";
import EnhancedTableHead from "../Recipes/EnhancedTableHead";
import ViewDialog from "./Dialogs/ViewDialog";
import UpdateDialog from "./Dialogs/UpdateDialog";
import CreateDialog from "./Dialogs/CreateDialog";
import ConfirmRecipeDeletion from "./Actions/ConfirmRecipeDeletion";
import ConfirmRecipeUpdate from "./Actions/ConfirmRecipeUpdate";
import ConfirmRecipeCreate from "./Actions/ConfirmRecipeCreate";


function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function getComparator(order, orderBy) {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    formContainer: {
        display: 'flex',
        flexWrap: 'wrap',
    }
}));

export default function EnhancedTable() {
    const classes = useStyles();
    const [order, setOrder] = useState('asc');
    const [orderBy, setOrderBy] = useState('title');
    const [page, setPage] = useState(0);
    const [dense, setDense] = useState(false);
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [recipesData, setRecipesData] = useState([]);
    const [specialsData, setSpecialsData] = useState([]);
    const [viewDialogIsOpen,setViewDialogIsOpen] = useState(false);
    const [createDialogIsOpen,setCreateDialogIsOpen] = useState(false);
    const [updateDialogIsOpen, setUpdateDialogIsOpen] = useState(false);

    const [fullWidth] = useState(true);
    
    const [dialogData, setDialogData] = useState({images: '', title: '', description: '', servings: '', prepTime: '', cookTime: '', ingredients: [], specials: [], directions: [], action: ''});
    const [updateFormData, setUpdateFormData] = useState(dialogData);
    const [createFormData, setCreateFormData] = useState({uuid: '', title: '', description: '', images: {full: '', medium: '', small: ''}, servings: '', prepTime: '', cookTime: '', postDate: (new Date()).toLocaleString(), editDate: (new Date()).toLocaleString(), ingredients: [{uuid: '', amount: 0, measurement: '', name: ''}], directions: [{instructions: '', optional: true}], action: ''});
    const [confirmDeleteRecipeDialogIsOpen, setConfirmDeleteRecipeDialogIsOpen] = useState(false);
    const [confirmDeleteRecipeDialogData, setConfirmDeleteRecipeDialogData] = useState({}); 
    const [confirmUpdateRecipeDialogIsOpen, setConfirmUpdateRecipeDialogIsOpen] = useState(false);
    const [confirmCreateRecipeDialogIsOpen, setConfirmCreateRecipeDialogIsOpen] = useState(false);

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleClickViewRecipeDetails = (event, row) => {
        setDialogData(row);
        handleViewDialogOpen();
    };

    const handleViewDialogOpen = (event, row) => {
        setViewDialogIsOpen(true);
    };

    const handleClickUpdateRecipeDetails = (event, row) => {
        setDialogData(row);
        handleUpdateDialogOpen(row);
    };

    const handleUpdateDialogOpen = (row) => {
        setUpdateDialogIsOpen(true);
        setUpdateFormData(row);
    };

    const renewRecipesData = () => {
        (async () => {
            getSpecials().then((items) => setSpecialsData(items));
            getRecipes().then((items) => setRecipesData(items));
        })();
    };

    const handleClickDeleteRecipeDetails = (event, row) => {
        setConfirmDeleteRecipeDialogIsOpen(true);
        setConfirmDeleteRecipeDialogData(row);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleChangeDense = (event) => {
        setDense(event.target.checked);
    };

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, recipesData.length - page * rowsPerPage);

    useEffect(() => {
        renewRecipesData();
    }, []);

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <EnhancedTableToolbar
                    setCreateDialogIsOpen={setCreateDialogIsOpen}
                />
                <TableContainer>
                    <Table
                        className={classes.table}
                        aria-labelledby="tableTitle"
                        size={dense ? 'small' : 'medium'}
                        aria-label="enhanced table"
                    >
                        <EnhancedTableHead
                            classes={classes}
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={handleRequestSort}
                        />
                        <TableBody>
                            {stableSort(recipesData, getComparator(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {
                                    return (
                                        <TableRow
                                            hover
                                            tabIndex={-1}
                                            key={row.title}
                                        >
                                            <TableCell align="right">
                                                <Avatar alt={row.title} src={row.images.medium} className={useStyles.large} />
                                            </TableCell>
                                            <TableCell component="th" scope="row">
                                                {row.title}
                                            </TableCell>
                                            <TableCell align="right">{row.description}</TableCell>
                                            <TableCell align="right">{row.servings}</TableCell>
                                            <TableCell align="right">{row.prepTime}</TableCell>
                                            <TableCell align="right">{row.cookTime}</TableCell>
                                            <TableCell align="center">
                                                <IconButton
                                                    color="primary"
                                                    aria-label="View Recipe"
                                                    title="View Recipe"
                                                    onClick={(event) => handleClickViewRecipeDetails(event, row)}
                                                >
                                                    <VisibilityIcon/>
                                                </IconButton>
                                                <IconButton
                                                    color="primary"
                                                    aria-label="Update Recipe"
                                                    title="Update Recipe"
                                                    onClick={(event) => handleClickUpdateRecipeDetails(event, row)}
                                                >
                                                    <EditIcon/>
                                              </IconButton>
                                              <IconButton
                                                    color="secondary"
                                                    aria-label="Delete Recipe"
                                                    title="Delete Recipe"
                                                    onClick={(event) => handleClickDeleteRecipeDetails(event, row)}
                                                >
                                                    <DeleteOutlineIcon/>
                                              </IconButton>
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                                    <TableCell colSpan={7} />
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={recipesData.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
            <FormControlLabel
                control={<Switch checked={dense} onChange={handleChangeDense} />}
                label="Dense padding"
            />
            <ViewDialog
                fullWidth={fullWidth}
                dialogData={dialogData}
                setViewDialogIsOpen={setViewDialogIsOpen}
                viewDialogIsOpen={viewDialogIsOpen}
                specialsData={specialsData}
            />
            <UpdateDialog
                fullWidth={fullWidth}
                updateFormData={updateFormData}
                setUpdateFormData={setUpdateFormData}
                updateDialogIsOpen={updateDialogIsOpen}
                setUpdateDialogIsOpen={setUpdateDialogIsOpen}
                setConfirmUpdateRecipeDialogIsOpen={setConfirmUpdateRecipeDialogIsOpen}
                renewRecipesData={renewRecipesData}
            />
            <CreateDialog
                fullWidth={fullWidth}
                createFormData={createFormData}
                setCreateFormData={setCreateFormData}
                createDialogIsOpen={createDialogIsOpen}
                setCreateDialogIsOpen={setCreateDialogIsOpen}
                setConfirmCreateRecipeDialogIsOpen={setConfirmCreateRecipeDialogIsOpen}
            />
            <ConfirmRecipeDeletion
                confirmDeleteRecipeDialogIsOpen={confirmDeleteRecipeDialogIsOpen}
                setConfirmDeleteRecipeDialogIsOpen={setConfirmDeleteRecipeDialogIsOpen}
                confirmDeleteRecipeDialogData={confirmDeleteRecipeDialogData}
                setRecipesData={setRecipesData}
                renewRecipesData={renewRecipesData}
            />
            <ConfirmRecipeUpdate
                confirmUpdateRecipeDialogIsOpen={confirmUpdateRecipeDialogIsOpen}
                setConfirmUpdateRecipeDialogIsOpen={setConfirmUpdateRecipeDialogIsOpen}
                setRecipesData={setRecipesData}
                updateFormData={updateFormData}
                setUpdateDialogIsOpen={setUpdateDialogIsOpen}
                specialsData={specialsData}
                recipesData={recipesData}
            />
            <ConfirmRecipeCreate
                confirmCreateRecipeDialogIsOpen={confirmCreateRecipeDialogIsOpen}
                setConfirmCreateRecipeDialogIsOpen={setConfirmCreateRecipeDialogIsOpen}
                createFormData={createFormData}
                setCreateFormData={setCreateFormData}
                setCreateDialogIsOpen={setCreateDialogIsOpen}
                renewRecipesData={renewRecipesData}
            />
        </div>
    );
}
