import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
// import AppBar from '@material-ui/core/AppBar';
// import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';


import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { green } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    postDate: {
        fontSize: '12px',
        fontWeight: 'normal',
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
    createFormMainContainer: {
        border: 'solid 1px #ddd',
        borderRadius: '6px',
        padding: '0 20px',
        marginBottom: '20px',
    },
    createFormMainContainerTitle: {
        position: 'relative',
        top: '-10px',
        background: '#fff',
        display: 'inline-block',
        padding: '0 6px',
    },
    createFormIngredientsContainer: {
        border: 'solid 1px #ddd',
        borderRadius: '6px',
        padding: '0 20px',
        marginBottom: '20px',
    },
    createFormIngredientsContainerTitle: {
        position: 'relative',
        top: '-10px',
        background: '#fff',
        display: 'inline-block',
        padding: '0 6px',
    },
    createFormDirectionsContainer: {
        border: 'solid 1px #ddd',
        borderRadius: '6px',
        padding: '0 20px',
        marginBottom: '20px',
    },
    createFormDirectionsContainerTitle: {
        position: 'relative',
        top: '-10px',
        background: '#fff',
        display: 'inline-block',
        padding: '0 6px',
        fontWeight: 'normal !important'
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    textFieldDirection: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '75%'
    },
    removeIngredientButton: {
        margin: '20px 0'
    },
    removeDirectionButton: {
        margin: '20px 0'
    }
}));

export default function CreateDialog(props) {
    const classes = useStyles();
    const { fullWidth, createFormData, setCreateFormData, createDialogIsOpen, setCreateDialogIsOpen, setConfirmCreateRecipeDialogIsOpen } = props;

    const [createDialogMaxWidth] = useState('md');

    const handleCreateDialogClose = (event, row) => {
        setCreateDialogIsOpen(false);
    };

    const handleCreateFormDataChange = (event) => {
        setCreateFormData({
            ...createFormData,

            // Trimming any whitespace
            [event.target.name]: event.target.value.trim()
        });
        createFormData[event.target.name] = event.target.value.trim();
    };

    const handleCreateFormDataIngredientsChange = (event) => {
        setCreateFormData({
            ...createFormData,

            // Trimming any whitespace
            [event.target.name]: event.target.value.trim()
        });
        createFormData.ingredients[event.target.id.split('-')[0]][event.target.id.split('-')[1]] = event.target.value.trim();
    };

    const handleClickCreateFormDataRemoveIngredients = (event) => {
        setCreateFormData({
            ...createFormData,
        });
        var button = event.target.closest('button');

        createFormData.ingredients.splice(button.id.split('-')[0], 1);
    };

    const handleClickCreateFormDataAddIngredients = (event) => {
        var newIngredient = {
            uuid: '',
            amount: 0,
            measurement: '',
            name: ''
        };

        createFormData.ingredients.push(newIngredient);
        setCreateFormData({
            ...createFormData,
        });
    };

    const handleCreateFormDataDirectionsChange = (event) => {
        setCreateFormData({
            ...createFormData,

            // Trimming any whitespace
            [event.target.name]: event.target.value.trim()
        });
        if (event.target.type === 'checkbox') {
            createFormData.directions[event.target.id.split('-')[0]][event.target.id.split('-')[1]] = event.target.checked;
        } else {
            createFormData.directions[event.target.id.split('-')[0]][event.target.id.split('-')[1]] = event.target.value.trim();
        }
    };

    const handleClickCreateFormDataRemoveDirections = (event) => {
        setCreateFormData({
            ...createFormData,
        });
        var button = event.target.closest('button');

        createFormData.directions.splice(button.id.split('-')[0], 1);
    };

    const handleClickCreateFormDataAddDirections = (event) => {
        var newDirection = {
            instructions: '',
            optional: true
        };

        createFormData.directions.push(newDirection);
        setCreateFormData({
            ...createFormData,
        });
    };

    const handleCreateRecipeSubmit= (event) => {
        setConfirmCreateRecipeDialogIsOpen(true);
    };

    return (
        <Dialog
            fullWidth={fullWidth}
            maxWidth={createDialogMaxWidth}
            open={createDialogIsOpen}
            onClose={handleCreateDialogClose}
            aria-labelledby="max-width-dialog-title"
        >
            <DialogTitle disableTypography>
                <Typography variant="h6">{createFormData.title ? 'Create New Recipe: ' + createFormData.title : 'Create New Recipe' }</Typography>
                <IconButton aria-label="close" className={classes.closeButton} onClick={handleCreateDialogClose}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleCreateRecipeSubmit}>
                    <div className={classes.createFormMainContainer}>
                        <Typography variant="caption" className={classes.createFormMainContainerTitle}>Main Details:</Typography>
                        <div>
                            <TextField
                                label="UUID"
                                className={classes.textField}
                                margin="normal"
                                InputLabelProps={{
                                shrink: true,
                                }}
                                variant="outlined"
                                value={createFormData.uuid}
                                name="uuid"
                                style={{ display: 'none' }}
                            />
                            <TextField
                                label="Title"
                                style={{ margin: 8 }}
                                placeholder="Add title"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                shrink: true,
                                }}
                                variant="outlined"
                                value={createFormData.title}
                                name="title"
                                onChange={handleCreateFormDataChange}
                                required
                            />
                            <TextField
                                label="Description"
                                style={{ margin: 8 }}
                                placeholder="Add description"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                shrink: true,
                                }}
                                variant="outlined"
                                value={createFormData.description}
                                name="description"
                                onChange={handleCreateFormDataChange}
                                required
                            />
                        </div>
                        <div>
                            <TextField
                                label="Servings"
                                placeholder="Add servings"
                                className={classes.textField}
                                helperText="in minutes"
                                margin="normal"
                                InputLabelProps={{
                                shrink: true,
                                }}
                                variant="outlined"
                                value={createFormData.servings}
                                name="servings"
                                onChange={handleCreateFormDataChange}
                                required
                            />
                            <TextField
                                label="Preparation Time"
                                placeholder="Add preparation time"
                                className={classes.textField}
                                helperText="in minutes"
                                margin="normal"
                                InputLabelProps={{
                                shrink: true,
                                }}
                                variant="outlined"
                                value={createFormData.prepTime}
                                name="prepTime"
                                onChange={handleCreateFormDataChange}
                                required
                            />
                            <TextField
                                label="Cooking Time"
                                placeholder="Add cooking time"
                                className={classes.textField}
                                helperText="in minutes"
                                margin="normal"
                                InputLabelProps={{
                                shrink: true,
                                }}
                                variant="outlined"
                                value={createFormData.cookTime}
                                name="cookTime"
                                onChange={handleCreateFormDataChange}
                                required
                            />
                        </div>
                    </div>
                    <div className={classes.createFormIngredientsContainer}>
                        <Typography variant="caption" className={classes.createFormIngredientsContainerTitle}>Ingredients:</Typography>
                            {createFormData.ingredients.map((ingredient, index) => {
                                return (
                                    <div key={index}>
                                        <TextField
                                            label="UUID"
                                            className={classes.textField}
                                            margin="normal"
                                            InputLabelProps={{
                                            shrink: true,
                                            }}
                                            variant="outlined"
                                            style={{ display: 'none' }}
                                            id={index + "-uuid"}
                                        />
                                        <TextField
                                            label="Name"
                                            placeholder="Add name"
                                            className={classes.textField}
                                            margin="normal"
                                            InputLabelProps={{
                                            shrink: true,
                                            }}
                                            variant="outlined"
                                            onChange={handleCreateFormDataIngredientsChange}
                                            id={index + "-name"}
                                        />
                                        <TextField
                                            label="Amount"
                                            placeholder="Add amount"
                                            className={classes.textField}
                                            margin="normal"
                                            InputLabelProps={{
                                            shrink: true,
                                            }}
                                            variant="outlined"
                                            onChange={handleCreateFormDataIngredientsChange}
                                            id={index + "-amount"}
                                        />
                                        <TextField
                                            label="Measurement"
                                            placeholder="Add measurement"
                                            className={classes.textField}
                                            margin="normal"
                                            InputLabelProps={{
                                            shrink: true,
                                            }}
                                            variant="outlined"
                                            onChange={handleCreateFormDataIngredientsChange}
                                            id={index + "-measurement"}
                                        />
                                        {(index > 0) &&
                                            <IconButton
                                                color="secondary"
                                                className={classes.removeIngredientButton}
                                                aria-label="Remove Ingredient"
                                                onClick={handleClickCreateFormDataRemoveIngredients}
                                                id={index + "-removeIngredient"}
                                            >
                                                <DeleteOutlineIcon/>
                                            </IconButton>
                                        }
                                        {(index < 1) &&
                                            <IconButton
                                                style={{ color: green[500] }}
                                                className={classes.removeIngredientButton}
                                                aria-label="Add Ingredient"
                                                onClick={(event) => handleClickCreateFormDataAddIngredients(event)}
                                            >
                                                <AddCircleOutlineIcon/>
                                            </IconButton>
                                        }
                                    </div>
                                );
                            })}
                    </div>
                    <div className={classes.createFormDirectionsContainer}>
                        <Typography className={classes.createFormDirectionsContainerTitle}>Directions:</Typography>
                        {createFormData.directions.map((direction, index) => {
                            return (
                                <div key={index}>
                                    <TextField
                                        label="Instructions"
                                        placeholder="Add instructions"
                                        className={classes.textFieldDirection}
                                        margin="normal"
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                        variant="outlined"
                                        onChange={handleCreateFormDataDirectionsChange}
                                        id={index + "-instructions"}
                                    />
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={direction.optional}
                                                onChange={handleCreateFormDataDirectionsChange}
                                                id={index + "-optional"}
                                                color="primary"
                                            />
                                        }
                                        label="Optional"
                                      />
                                    
                                    {(index > 0) &&
                                        <IconButton
                                            color="secondary"
                                            className={classes.removeDirectionButton}
                                            aria-label="Remove Direction"
                                            onClick={handleClickCreateFormDataRemoveDirections}
                                            id={index + "-removeDirection"}
                                        >
                                            <DeleteOutlineIcon/>
                                        </IconButton>
                                    }
                                    {(index < 1) &&
                                        <IconButton
                                            style={{ color: green[500] }}
                                            className={classes.removeDirectionButton}
                                            aria-label="Add Direction"
                                            onClick={(event) => handleClickCreateFormDataAddDirections(event)}
                                        >
                                            <AddCircleOutlineIcon/>
                                        </IconButton>
                                    }
                                </div>
                            );
                        })}
                    </div>
                </form>
            </DialogContent>
            <DialogActions>
                <Button onClick={(event) => handleCreateDialogClose(event)} color="primary">
                    Cancel
                </Button>
                <Button onClick={handleCreateRecipeSubmit} color="primary">
                    Save Changes
                </Button>
            </DialogActions>
        </Dialog>
    );
}

CreateDialog.propTypes = {
    fullWidth: PropTypes.bool,
    createFormData: PropTypes.object,
    setCreateFormData: PropTypes.func,
    createDialogIsOpen: PropTypes.bool,
    setCreateDialogIsOpen: PropTypes.func,
    setConfirmCreateRecipeDialogIsOpen: PropTypes.func
};
