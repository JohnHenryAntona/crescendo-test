import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
// import AppBar from '@material-ui/core/AppBar';
// import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';


import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { green } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    postDate: {
        fontSize: '12px',
        fontWeight: 'normal',
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
    updateFormMainContainer: {
        border: 'solid 1px #ddd',
        borderRadius: '6px',
        padding: '0 20px',
        marginBottom: '20px',
    },
    updateFormMainContainerTitle: {
        position: 'relative',
        top: '-10px',
        background: '#fff',
        display: 'inline-block',
        padding: '0 6px',
    },
    updateFormIngredientsContainer: {
        border: 'solid 1px #ddd',
        borderRadius: '6px',
        padding: '0 20px',
        marginBottom: '20px',
    },
    updateFormIngredientsContainerTitle: {
        position: 'relative',
        top: '-10px',
        background: '#fff',
        display: 'inline-block',
        padding: '0 6px',
    },
    updateFormDirectionsContainer: {
        border: 'solid 1px #ddd',
        borderRadius: '6px',
        padding: '0 20px',
        marginBottom: '20px',
    },
    updateFormDirectionsContainerTitle: {
        position: 'relative',
        top: '-10px',
        background: '#fff',
        display: 'inline-block',
        padding: '0 6px',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    textFieldDirection: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '75%'
    },
    removeIngredientButton: {
        margin: '20px 0'
    },
    removeDirectionButton: {
        margin: '20px 0'
    }
}));

export default function UpdateDialog(props) {
    const classes = useStyles();
    const { fullWidth, updateFormData, setUpdateFormData, updateDialogIsOpen, setUpdateDialogIsOpen, setConfirmUpdateRecipeDialogIsOpen, renewRecipesData } = props;

    const [updateDialogMaxWidth] = useState('md');

    const handleUpdateDialogClose = (event, row) => {
        setUpdateDialogIsOpen(false);
        renewRecipesData();
    };

    const handleUpdateFormDataChange = (event) => {
        setUpdateFormData({
            ...updateFormData,

            // Trimming any whitespace
            [event.target.name]: event.target.value.trim()
        });
        updateFormData[event.target.name] = event.target.value.trim();
    };

    const handleUpdateFormDataIngredientsChange = (event) => {
        setUpdateFormData({
            ...updateFormData,

            // Trimming any whitespace
            [event.target.name]: event.target.value.trim()
        });
        updateFormData.ingredients[event.target.id.split('-')[0]][event.target.id.split('-')[1]] = event.target.value.trim();
    };

    const handleClickUpdateFormDataRemoveIngredients = (event) => {
        setUpdateFormData({
            ...updateFormData,
        });
        var button = event.target.closest('button');

        updateFormData.ingredients.splice(button.id.split('-')[0], 1);
    };

    const handleClickUpdateFormDataAddIngredients = (event) => {
        var newIngredient = {
            uuid: '',
            amount: 0,
            measurement: '',
            name: ''
        };

        updateFormData.ingredients.push(newIngredient);
        setUpdateFormData({
            ...updateFormData,
        });
    };

    const handleUpdateFormDataDirectionsChange = (event) => {
        setUpdateFormData({
            ...updateFormData,

            // Trimming any whitespace
            [event.target.name]: event.target.value.trim()
        });
        if (event.target.type === 'checkbox') {
            updateFormData.directions[event.target.id.split('-')[0]][event.target.id.split('-')[1]] = event.target.checked;
        } else {
            updateFormData.directions[event.target.id.split('-')[0]][event.target.id.split('-')[1]] = event.target.value.trim();
        }
    };

    const handleClickUpdateFormDataRemoveDirections = (event) => {
        setUpdateFormData({
            ...updateFormData,
        });
        var button = event.target.closest('button');

        updateFormData.directions.splice(button.id.split('-')[0], 1);
    };

    const handleUpdateRecipeSubmit= (event) => {
        setConfirmUpdateRecipeDialogIsOpen(true);
    };

    return (
        <Dialog
            fullWidth={fullWidth}
            maxWidth={updateDialogMaxWidth}
            open={updateDialogIsOpen}
            onClose={handleUpdateDialogClose}
            aria-labelledby="max-width-dialog-title"
        >
            <DialogTitle disableTypography>
                <Typography variant="h6">Update {updateFormData.title}</Typography>
                <Typography className={classes.postDate}>{updateFormData.postDate}</Typography>
                <IconButton aria-label="close" className={classes.closeButton} onClick={handleUpdateDialogClose}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleUpdateRecipeSubmit}>
                    <div className={classes.updateFormMainContainer}>
                        <Typography variant="caption" className={classes.updateFormMainContainerTitle}>Main Details:</Typography>
                        <div>
                            <TextField
                                label="UUID"
                                className={classes.textField}
                                margin="normal"
                                InputLabelProps={{
                                shrink: true,
                                }}
                                variant="outlined"
                                value={updateFormData.uuid}
                                name="uuid"
                                style={{ display: 'none' }}
                            />
                            <TextField
                                label="Title"
                                style={{ margin: 8 }}
                                placeholder="Add title"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                shrink: true,
                                }}
                                variant="outlined"
                                value={updateFormData.title}
                                name="title"
                                onChange={handleUpdateFormDataChange}
                                required
                            />
                            <TextField
                                label="Description"
                                style={{ margin: 8 }}
                                placeholder="Add description"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                shrink: true,
                                }}
                                variant="outlined"
                                value={updateFormData.description}
                                name="description"
                                onChange={handleUpdateFormDataChange}
                                required
                            />
                        </div>
                        <div>
                            <TextField
                                label="Servings"
                                placeholder="Add servings"
                                className={classes.textField}
                                helperText="in minutes"
                                margin="normal"
                                InputLabelProps={{
                                shrink: true,
                                }}
                                variant="outlined"
                                value={updateFormData.servings}
                                name="servings"
                                onChange={handleUpdateFormDataChange}
                                required
                            />
                            <TextField
                                label="Preparation Time"
                                placeholder="Add preparation time"
                                className={classes.textField}
                                helperText="in minutes"
                                margin="normal"
                                InputLabelProps={{
                                shrink: true,
                                }}
                                variant="outlined"
                                value={updateFormData.prepTime}
                                name="prepTime"
                                onChange={handleUpdateFormDataChange}
                                required
                            />
                            <TextField
                                label="Cooking Time"
                                placeholder="Add cooking time"
                                className={classes.textField}
                                helperText="in minutes"
                                margin="normal"
                                InputLabelProps={{
                                shrink: true,
                                }}
                                variant="outlined"
                                value={updateFormData.cookTime}
                                name="cookTime"
                                onChange={handleUpdateFormDataChange}
                                required
                            />
                        </div>
                    </div>
                    <div className={classes.updateFormIngredientsContainer}>
                        <Typography variant="caption" className={classes.updateFormIngredientsContainerTitle}>Ingredients:</Typography>
                        {updateFormData.ingredients.map((ingredient, index) => {
                                return (
                                    <div key={index}>
                                        <TextField
                                            label="UUID"
                                            className={classes.textField}
                                            margin="normal"
                                            InputLabelProps={{
                                            shrink: true,
                                            }}
                                            variant="outlined"
                                            value={ingredient.uuid}
                                            style={{ display: 'none' }}
                                            id={index + "-uuid"}
                                        />
                                        <TextField
                                            label="Name"
                                            placeholder="Add name"
                                            className={classes.textField}
                                            margin="normal"
                                            InputLabelProps={{
                                            shrink: true,
                                            }}
                                            variant="outlined"
                                            value={ingredient.name}
                                            onChange={handleUpdateFormDataIngredientsChange}
                                            id={index + "-name"}
                                        />
                                        <TextField
                                            label="Amount"
                                            placeholder="Add amount"
                                            className={classes.textField}
                                            margin="normal"
                                            InputLabelProps={{
                                            shrink: true,
                                            }}
                                            variant="outlined"
                                            value={ingredient.amount}
                                            onChange={handleUpdateFormDataIngredientsChange}
                                            id={index + "-amount"}
                                        />
                                        <TextField
                                            label="Measurement"
                                            placeholder="Add measurement"
                                            className={classes.textField}
                                            margin="normal"
                                            InputLabelProps={{
                                            shrink: true,
                                            }}
                                            variant="outlined"
                                            value={ingredient.measurement}
                                            onChange={handleUpdateFormDataIngredientsChange}
                                            id={index + "-measurement"}
                                        />
                                        <IconButton
                                            color="secondary"
                                            className={classes.removeIngredientButton}
                                            aria-label="Remove Ingredient"
                                            onClick={handleClickUpdateFormDataRemoveIngredients}
                                            id={index + "-removeIngredient"}
                                        >
                                            <DeleteOutlineIcon/>
                                      </IconButton>
                                      {(index === updateFormData.ingredients.length - 1) &&
                                          <IconButton
                                                style={{ color: green[500] }}
                                                className={classes.removeIngredientButton}
                                                aria-label="Add Ingredient"
                                                onClick={(event) => handleClickUpdateFormDataAddIngredients(event)}
                                            >
                                                <AddCircleOutlineIcon/>
                                          </IconButton>
                                      }
                                    </div>
                                );
                            })}
                    </div>
                    <div className={classes.updateFormDirectionsContainer}>
                        <Typography className={classes.updateFormDirectionsContainerTitle}>Directions:</Typography>
                        {updateFormData.directions.map((direction, index) => {
                            return (
                                <div key={index}>
                                    <TextField
                                        label="Instructions"
                                        placeholder="Add instructions"
                                        className={classes.textFieldDirection}
                                        margin="normal"
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                        variant="outlined"
                                        value={direction.instructions}
                                        onChange={handleUpdateFormDataDirectionsChange}
                                        id={index + "-instructions"}
                                    />
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={direction.optional}
                                                onChange={handleUpdateFormDataDirectionsChange}
                                                id={index + "-optional"}
                                                color="primary"
                                            />
                                        }
                                        label="Optional"
                                        value={direction.optional}
                                      />
                                    <IconButton
                                        color="secondary"
                                        className={classes.removeDirectionButton}
                                        aria-label="Remove Direction"
                                        onClick={handleClickUpdateFormDataRemoveDirections}
                                        id={index + "-removeDirection"}
                                    >
                                        <DeleteOutlineIcon/>
                                    </IconButton>
                                </div>
                            );
                        })}
                    </div>
                </form>
            </DialogContent>
            <DialogActions>
                <Button onClick={(event) => handleUpdateDialogClose(event)} color="primary">
                    Cancel
                </Button>
                <Button onClick={handleUpdateRecipeSubmit} color="primary">
                    Save Changes
                </Button>
            </DialogActions>
        </Dialog>
    );
}

UpdateDialog.propTypes = {
    fullWidth: PropTypes.bool,
    updateFormData: PropTypes.object,
    setUpdateFormData: PropTypes.func,
    updateDialogIsOpen: PropTypes.bool,
    setUpdateDialogIsOpen: PropTypes.func,
    setConfirmUpdateRecipeDialogIsOpen: PropTypes.func,
    renewRecipesData: PropTypes.func
};
