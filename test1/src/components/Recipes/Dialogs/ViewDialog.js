import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
// import AppBar from '@material-ui/core/AppBar';
// import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';


import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    postDate: {
        fontSize: '12px',
        fontWeight: 'normal',
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
    thumbnailContainer: {
        textAlign: 'center'
    },
    thumbnail: {
        width: '100%',
        paddingBottom: '2rem'
    },
    specialsContainer: {
        backgroundColor: '#b7d6a4',
        padding: '1rem',
        width: '90%'
    },
}));

function checkSpecials(specials, ingredientId) {
    return specials.filter(function(special) {
        return special.ingredientId === ingredientId;
    });
}

export default function ViewDialog(props) {
    const classes = useStyles();
    const { fullWidth, dialogData, setViewDialogIsOpen, viewDialogIsOpen, specialsData } = props;

    const [viewDialogMaxWidth] = useState('sm');

    const handleViewDialogClose = (event, row) => {
        setViewDialogIsOpen(false);
    };

    return (
        <Dialog
            fullWidth={fullWidth}
            maxWidth={viewDialogMaxWidth}
            open={viewDialogIsOpen}
            onClose={handleViewDialogClose}
            aria-labelledby="max-width-dialog-title"
        >
            <DialogTitle disableTypography>
                <Typography variant="h6">{dialogData.title}</Typography>
                <Typography className={classes.postDate}>{dialogData.postDate}</Typography>
                <IconButton aria-label="close" className={classes.closeButton} onClick={handleViewDialogClose}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent>
                <div className={classes.thumbnailContainer}>
                    <img alt={dialogData.title} src={dialogData.images.medium} align="center" className={classes.thumbnail}/>
                </div>
                <div>
                    <Typography variant="subtitle1">Description: <Typography variant="caption">{dialogData.description}</Typography></Typography>

                </div>
                <div>
                    <Typography variant="subtitle1">Servings: <Typography variant="caption">good for {dialogData.servings} person{dialogData.servings > 1 ? 's' : ''}</Typography></Typography>
                </div>
                <div>
                    <Typography variant="subtitle1">Preparation Time: <Typography variant="caption">{dialogData.prepTime} min{dialogData.prepTime > 1 ? 's' : ''}</Typography></Typography>
                </div>
                <div>
                    <Typography variant="subtitle1">Cooking Time: <Typography variant="caption">{dialogData.cookTime} min{dialogData.cookTime > 1 ? 's' : ''}</Typography></Typography>
                </div>
                <div>
                    <Typography variant="subtitle1" weight="bold">Ingredients:</Typography>
                    <ul>
                        {dialogData.ingredients.map((ingredient, index) => {
                            return (
                                <li key={index}>
                                    {ingredient.amount} {ingredient.measurement} {ingredient.name}
                                    {   
                                        (checkSpecials(specialsData, ingredient.uuid).length > 0)
                                        ?
                                            <div className={classes.specialsContainer}>
                                                <Typography variant="subtitle1">Specials:</Typography>
                                                <ul>
                                                    {checkSpecials(specialsData, ingredient.uuid).map((special, index) => {
                                                        return (
                                                            <li key={index} id="specials-container">
                                                                <Typography variant="subtitle1" style={{ textTransform: 'uppercase'}}>{special.type}: <Typography variant="caption">{special.title}</Typography></Typography>
                                                                <Typography variant="subtitle1">Details: <Typography variant="caption" style={{ textTransform: 'capitalize'}}>{special.text}</Typography></Typography>
                                                            </li>
                                                        );
                                                    })}
                                                </ul>
                                            </div>
                                        : ''
                                    }
                                </li>
                            );
                        })}
                    </ul>
                </div>
                <div>
                    <Typography weight="bold">Directions:</Typography>
                    <ol>
                        {dialogData.directions.map((direction, index) => {
                            return (
                                <li key={index}>{direction.instructions} {direction.optional ? '(Optional)' : ''}</li>
                            );
                        })}
                    </ol>
                </div>
            </DialogContent>
        </Dialog>
    );
}

ViewDialog.propTypes = {
    fullWidth: PropTypes.bool,
    dialogData: PropTypes.object,
    setViewDialogIsOpen: PropTypes.func,
    viewDialogIsOpen: PropTypes.bool,
    specialsData: PropTypes.array
};
