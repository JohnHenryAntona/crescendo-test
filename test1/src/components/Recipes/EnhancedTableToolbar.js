import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { green } from '@material-ui/core/colors';

const useToolbarStyles = makeStyles((theme) => ({
    title: {
        flex: '1 1 100%',
    },
}));

export default function EnhancedTableToolbar(props) {
    const classes = useToolbarStyles();
    const { setCreateDialogIsOpen } = props;

    const handleCreateDialogOpen = (event) => {
        setCreateDialogIsOpen(true);
        // setCreateFormData(row);
    };

    return (
        <Toolbar>
            <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                Recipes
            </Typography>
            <Tooltip title="Create New Recipe">
                <IconButton
                    aria-label="filter list"
                    style={{ color: green[500] }}
                    onClick={(event) => handleCreateDialogOpen(event)}
                >
                    <AddCircleOutlineIcon />
                </IconButton>
            </Tooltip>
        </Toolbar>
    );
}

EnhancedTableToolbar.propTypes = {
    setCreateDialogIsOpen: PropTypes.func
};